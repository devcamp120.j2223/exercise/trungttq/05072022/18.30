import { Container, Grid, TableContainer, Table, TableHead, TableBody, TableRow, TableCell, Button, Pagination, Modal, Box, Typography, InputLabel, Select, MenuItem } from "@mui/material";
import { useEffect, useState } from "react";

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

function Datatable() {
    const [rows, setRows] = useState([]);

    // Limit: Số lượng bản ghi trên 1 trang

    const [limit, setLimit] = useState(10);
    // Số trang: Tổng số lượng sản phẩm / limit - Số lớn hơn gần nhất
    const [noPage, setNoPage] = useState(0);

    // Trang hiện tại
    const [page, setPage] = useState(1);

    const [open, setOpen] = useState(false);

    const [userinfo, setUserInfo] = useState([]);


    const handleClose = () => setOpen(false);



    const getData = async (url) => {
        const response = await fetch(url);

        const data = await response.json();

        return data;
    }

    const clickUpdate = (userInfo) => {
        
        console.log(userInfo);
    }

    const clickDelete = (userInfo) => {
        
        console.log(userInfo);
    }

    const onChangePagination = (event, value) => {
        setPage(value);
    }

    const changeNumberPageHandler = (event) => {
        console.log(event.target.value)
        setLimit(event.target.value)

    }

    useEffect(() => {
        getData("http://42.115.221.44:8080/devcamp-register-java-api/users")
            .then((res) => {
                setNoPage(Math.ceil(res.length / limit));

                setRows(res.slice((page - 1) * limit, page * limit));
            })
            .catch((error) => {
                console.error(error.message)
            })
    }, [page, limit]);

    return (
        <Container>


            <Grid container>
                <Grid item>
                    <Grid>
                        <InputLabel id="demo-simple-select-label">Entries</InputLabel>
                        <Select
                            label="Entry"
                            onChange={changeNumberPageHandler}
                        >
                            <MenuItem value={5} >5</MenuItem>
                            <MenuItem value={10}>10</MenuItem>
                            <MenuItem value={15}>15</MenuItem>
                        </Select>
                    </Grid>
                </Grid>



                <Grid item>
                    <TableContainer>
                        <Table sx={{ minWidth: 650 }} aria-label="simple table">
                            <TableHead>
                                <TableRow>
                                    <TableCell align="center">STT</TableCell>
                                    <TableCell align="center">mã người dùng</TableCell>
                                    <TableCell align="center">firstname</TableCell>
                                    <TableCell align="center">lastname</TableCell>
                                    <TableCell align="center">country</TableCell>
                                    <TableCell align="center">subject</TableCell>
                                    <TableCell align="center">customer type</TableCell>
                                    <TableCell align="center">register status</TableCell>
                                    <TableCell align="center">action</TableCell>

                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {
                                    rows.map((row, index) => {
                                        return (
                                            <TableRow key={index}>
                                                <TableCell align="center">{index + 1}</TableCell>
                                                <TableCell align="center">{row.id}</TableCell>
                                                <TableCell align="center">{row.firstname}</TableCell>
                                                <TableCell>{row.lastname}</TableCell>
                                                <TableCell>{row.country}</TableCell>
                                                <TableCell>{row.subject}</TableCell>
                                                <TableCell>{row.customerType}</TableCell>
                                                <TableCell>{row.registerStatus}</TableCell>
                                                <TableCell align="center"><Button variant="contained" onClick={() => { clickUpdate(row.id) }} sx={{margin: "1px"}}>Sửa</Button>
                                                    <Button variant="contained" onClick={() => { clickDelete(row.id) }} >Xóa</Button>
                                                </TableCell>
                                            </TableRow>
                                        )
                                    })
                                }
                            </TableBody>
                        </Table>
                    </TableContainer>
                </Grid>
            </Grid>
            <Grid container mt={3} mb={2} justifyContent="flex-end">
                <Grid item>
                    <Pagination count={noPage} defaultPage={page} onChange={onChangePagination} />
                </Grid>
            </Grid>

        </Container>
    )
}

export default Datatable;